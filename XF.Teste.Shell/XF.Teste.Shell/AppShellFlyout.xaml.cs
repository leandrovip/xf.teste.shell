﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XF.Teste.Shell
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppShellFlyout : Xamarin.Forms.Shell
    {
        public AppShellFlyout()
        {
            InitializeComponent();
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            await Application.Current.MainPage.DisplayAlert("Alert", "No internet connection", "Ok");
        }
    }
}