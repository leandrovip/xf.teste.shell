﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XF.Teste.Shell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeView : ContentPage
    {
        public HomeView()
        {
            InitializeComponent();
        }

        private async void Button_OnClicked(object sender, EventArgs e)
        {
            await DisplayAlert("Atenção", "Esta é uma mensagem da Home", "Aceitar", "Cancelar");
        }
    }
}