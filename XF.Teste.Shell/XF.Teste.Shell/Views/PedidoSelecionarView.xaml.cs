﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XF.Teste.Shell.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PedidoSelecionarView : ContentPage
    {
        public PedidoSelecionarView()
        {
            InitializeComponent();
        }
    }
}